package com.acerola.pdfword.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.acerola.pdfword.service.PdfToWord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: pdf-word
 * @description: pdf转word
 * @author: Acerola
 * @create: 2022-02-15 20:59
 **/
@Api(tags = "控制层")
@RestController
public class Convert {
    @Autowired
    private PdfToWord pdf;



    @ApiOperation(value = "pdf转word")
    @PostMapping("/convert")
    public String convert( String path, String url) throws IOException, InterruptedException {
        MultipartFile file;
        System.out.println(path);
        System.out.println(url);
        String pdftoword = pdf.pdftoword(path);
        System.out.println("*************************" + pdftoword);
        return url;
    }
}
