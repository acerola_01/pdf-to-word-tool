package com.acerola.pdfword.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: pdf-word
 * @description: 上传
 * @author: Acerola
 * @create: 2022-02-17 14:40
 **/

@RestController
public class upload {

    @Value("${server.port}")
    private String port;

    @Value("${upload.ip}")
    private String ip;

    @Value("${upload.filePath}")
    private String filePath;

    @PostMapping("/upload")
    private Map upload(@RequestPart("file") MultipartFile file, HttpServletResponse response) throws IOException, InterruptedException {
        /**
         * 文件上传
         */

        String filename = file.getOriginalFilename();   //获取文件名称
        //定义唯一标识符
        String uuid = IdUtil.fastSimpleUUID();
        String rootFilePath = filePath + uuid + "_" + filename;  //获取文件存储路劲
        File file1 = FileUtil.writeBytes(file.getBytes(), rootFilePath);

        System.out.println(file1.getAbsolutePath());
        System.out.println(file1.getPath());
        System.out.println(file1.getCanonicalPath());
        Map map = new HashMap<>();
        map.put("name", filename);
        map.put("downloadUrl", ip + ":" + port + "/file/" + uuid);
        map.put("rootFilePath", rootFilePath);
        System.out.println(map);
        return map;
    }

}
