package com.acerola.pdfword.service;

import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class FileDeleteTest {
	//删除文件和目录
	public void clearFiles(String workspaceRootPath){
		File file = new File(workspaceRootPath);
		if(file.exists()){
			deleteFile(file);
		}
	}
	public void deleteFile(File file){
		if(file.isDirectory()){
			File[] files = file.listFiles();
			for(int i=0; i<files.length; i++){
				deleteFile(files[i]);
			}
		}
		file.delete();
	}
}
