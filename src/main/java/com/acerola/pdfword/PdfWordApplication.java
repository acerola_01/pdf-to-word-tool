package com.acerola.pdfword;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
public class PdfWordApplication {

    public static void main(String[] args) {
        SpringApplication.run(PdfWordApplication.class, args);
    }

}
